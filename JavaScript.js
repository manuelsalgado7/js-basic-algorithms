//Variables

//1.1

let myFavoriteHero = "Hulk";

//1.2

let x = 10;

//1.3

let h = 5;
let y = 10;

//1.4

let z = h + y;

//Advanced Variables

//2.1
const character = { name: "Jack Sparrow", age: 10 };
character.age = 10;

//2.2
firstName = "Jon";
lastName = "Snow";
age = 24;

console.log(
  "I'm " +
    firstName +
    " " +
    lastName +
    ", I'm " +
    age +
    " years old and I like wolves."
);

//2.3
const toy1 = { name: "Buss myYear", price: 19 };
const toy2 = { name: "Rallo mcKing", price: 29 };
console.log(toy1.price + toy2.price);

//2.4
let globalBasePrice = 10000;
const car1 = { name: "BMW m&m", basePrice: 50000, finalPrice: 60000 };
const car2 = { name: "Chevrolet Corbina", basePrice: 70000, finalPrice: 80000 };

globalBasePrice = 25000;
car1.finalPrice = car1.basePrice + globalBasePrice;
car2.finalPrice = car2.basePrice + globalBasePrice;

//Operators

//3.1
alert(10 * 5);

//3.2
alert(10 * 2);

//3.3
alert(15 % 9);

//3.4
let y = 10;
let z = 5;
let x = z + y;

//3.5
let y = 10;
let z = 5;
let x = z * y;

//Iteration

//4.1

const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
console.log(avengers[0]);

//4.2

const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
avengers[0] = "IRONMAN";

//4.3

const avengers = ["HULK", "SPIDERMAN", "BLACK PANTHER"];
alert(avengers.length);

//4.4

const rickAndMortyCharacters = ["Rick", "Beth", "Jerry"];
rickAndMortyCharacters.push("Morty");
rickAndMortyCharacters.push("Summer");

//4.5

const rickAndMortyCharacters = [
  "Rick",
  "Beth",
  "Jerry",
  "Morty",
  "Summer",
  "Lapiz Lopez",
];
rickAndMortyCharacters.pop();
console.log(rickAndMortyCharacters[0]);
console.log(rickAndMortyCharacters[rickAndMortyCharacters.length - 1]);

//4.6

const rickAndMortyCharacters = [
  "Rick",
  "Beth",
  "Jerry",
  "Morty",
  "Summer",
  "Lapiz Lopez",
];
rickAndMortyCharacters.splice(1, 1);
console.log(rickAndMortyCharacters);

//Conditionals

const number1 = 10;
const number2 = 20;
const number3 = 2;

if (number1 === 10) {
  console.log("number1 is strictly equal to 10");
}

if (number2 / number1 === 2) {
  console.log("number2 divided by number1 equals 2");
}

if (number1 !== number2) {
  console.log("number1 is strictly different from number2");
}

if (number3 !== number1) {
  console.log("number3 is distinct number1");
}

if (number3 === number1) {
  console.log("number3 times 5 equals number1");
}

if (number3 * 5 === number1 && number1 * 2 === number2) {
  console.log(
    "number3 times 5 equals number1 AND number1 times 2 equals number2"
  );
}

if (number2 * 2 === number1 || number1 * 5 === number3) {
  console.log(
    "number2 times 2 equals number1 OR number1 times 5 equals number3"
  );
}

//Loops

//6.1

for (let i = 0; i <= 9; i++) {
  console.log(i);
}

//6.2

for (let i = 0; i <= 9; i++) {
  console.log(i);
}

//6.3

for (let i = 0; i <= 10; i++) {
  if (i < 10) {
    console.log("Trying to sleep");
  }
  console.log("Sleeping!");
}
